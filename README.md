`async-nio` adapts Java NIO to Kotlin Coroutine.

# Example

Using `async-nio` and Kotlin Coroutine you can use asynchronous NIO without any callback.

Every suspending method starts with a lowercase "a", so `accept` becomes `aAccept` and `read` becomes `aRead`.

```kotlin
import io.gitlab.asynk.nio.aAccept
import io.gitlab.asynk.nio.aRead
import io.gitlab.asynk.nio.aWrite
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousByteChannel
import java.nio.channels.AsynchronousServerSocketChannel
import java.nio.channels.AsynchronousSocketChannel

suspend fun main() {
    val serverSocketChannel: AsynchronousServerSocketChannel = AsynchronousServerSocketChannel.open().bind(InetSocketAddress(5000))
    println("Waiting on $serverSocketChannel")
    val socketChannel: AsynchronousSocketChannel = serverSocketChannel.aAccept()

    println("Read data from $socketChannel")
    val buffer: ByteBuffer = ByteBuffer.allocate(10)
    // read data from network
    while (socketChannel.aRead(buffer) >= 0) {
        buffer.flip()
        // send the same data to network
        socketChannel.aWriteFully(buffer)
        buffer.clear()
    }

    println("Bye bye")
}

// use async-nio to build your tools
suspend inline fun AsynchronousByteChannel.aWriteFully(src: ByteBuffer) {
    while (src.hasRemaining()) aWrite(src)
}
```

You can test di example using telnet: `telnet localhost 5000`

# Performance

This tiny library defines a single object, any other extension methods are inlined at compilation time.

Every NIO asynchronous method requires two parameters: `attachment` and `handler`.
The `attachment` will be the current coroutine continuation, it is required for coroutine machinery, optimized by Kotlin compiler and smaller than a typical stack. Plus: it is reusable!
The `handler` is a singleton object, no extra allocation cost.

# Adding a dependency

This project requires Kotlin 1.3.

```groovy
repositories {
    maven { url "https://dl.bintray.com/fvasco/io.gitlab.asynk" }
}

dependencies {
    compile 'io.gitlab.asynk:nio:0.0.1'
}
```

This project does not require the `kotlinx-coroutine` dependency, fell free to include it if you wish.


# See also

You can consider similar libraries instead of this.
`asynk-nio` is a tiny utility around Java NIO, nothing more.

[kotlinx-io](https://github.com/Kotlin/kotlinx-io/) is a multiplatform library for processing binary data, working with memory blocks, interacting with the platform, and performing other low level operations.

[ktor](https://ktor.io/) is a framework for quickly creating connected applications in Kotlin with minimal effort.

[kotlinx.coroutines](https://github.com/Kotlin/kotlinx.coroutines/) is a library support for Kotlin coroutines.
