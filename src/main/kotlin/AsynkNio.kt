@file:Suppress("RedundantVisibilityModifier", "unused")

package io.gitlab.asynk.nio

import java.net.SocketAddress
import java.nio.ByteBuffer
import java.nio.channels.*
import java.util.concurrent.TimeUnit
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

public suspend inline fun AsynchronousByteChannel.aWrite(src: ByteBuffer): Int =
        nioAwait { attachment, handler ->
            write(src, attachment, handler)
        }

public suspend inline fun AsynchronousByteChannel.aRead(dst: ByteBuffer): Int =
        nioAwait { attachment, handler ->
            read(dst, attachment, handler)
        }

public suspend inline fun AsynchronousFileChannel.aLock(): FileLock =
        nioAwait { attachment, handler ->
            lock(attachment, handler)
        }

public suspend inline fun AsynchronousFileChannel.aLock(position: Long, size: Long, shared: Boolean): FileLock =
        nioAwait { attachment, handler ->
            lock(position, size, shared, attachment, handler)
        }

public suspend inline fun AsynchronousFileChannel.aWrite(dst: ByteBuffer, position: Long): Int =
        nioAwait { attachment, handler ->
            write(dst, position, attachment, handler)
        }

public suspend inline fun AsynchronousFileChannel.aRead(src: ByteBuffer, position: Long): Int =
        nioAwait { attachment, handler ->
            read(src, position, attachment, handler)
        }

public suspend inline fun AsynchronousServerSocketChannel.aAccept(): AsynchronousSocketChannel =
        nioAwait { attachment, handler ->
            accept(attachment, handler)
        }

public suspend inline fun AsynchronousSocketChannel.aWrite(dst: ByteBuffer, timeout: Long, unit: TimeUnit): Int =
        nioAwait { attachment, handler ->
            write(dst, timeout, unit, attachment, handler)
        }

public suspend inline fun AsynchronousSocketChannel.aWrite(dsts: Array<ByteBuffer>, offset: Int, length: Int, timeout: Long, unit: TimeUnit): Long =
        nioAwait { attachment, handler ->
            write(dsts, offset, length, timeout, unit, attachment, handler)
        }

public suspend inline fun AsynchronousSocketChannel.aRead(src: ByteBuffer, timeout: Long, unit: TimeUnit): Int =
        nioAwait { attachment, handler ->
            read(src, timeout, unit, attachment, handler)
        }

public suspend inline fun AsynchronousSocketChannel.aRead(srcs: Array<ByteBuffer>, offset: Int, length: Int, timeout: Long, unit: TimeUnit): Long =
        nioAwait { attachment, handler ->
            read(srcs, offset, length, timeout, unit, attachment, handler)
        }

public suspend inline fun AsynchronousSocketChannel.aConnect(remote: SocketAddress) {
    nioAwait<Void> { attachment, handler ->
        connect(remote, attachment, handler)
    }
}

/**
 * Suspends the currently running coroutine
 * and provides to [block] the required parameters to invoke any asynchronous NIO method.
 *
 * The [block]'s parameters are:
 * attachment: the second to last argument of any NIO asynchronous method;
 * completionHandler: the last argument of any NIO asynchronous method.
 */
public suspend inline fun <V : Any> nioAwait(crossinline block: (attachment: Any, completionHandler: CompletionHandler<V, Any>) -> Unit): V =
        suspendCoroutine { continuation ->
            @Suppress("UNCHECKED_CAST")
            block(continuation, ContinuationCompletionHandler as CompletionHandler<V, Any>)
        }

@PublishedApi
internal object ContinuationCompletionHandler : CompletionHandler<Any, Continuation<Any>> {
    override fun completed(result: Any, attachment: Continuation<Any>) {
        attachment.resume(result)
    }

    override fun failed(throwable: Throwable, attachment: Continuation<Any>) {
        attachment.resumeWithException(throwable)
    }
}
