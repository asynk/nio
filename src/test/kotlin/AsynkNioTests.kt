package io.gitlab.asynk.nio

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousByteChannel
import java.nio.channels.CompletionHandler

class AsynkNioTests {

    @Test
    fun `nioAwait ok`() = runTest {
        nioAwait<Unit> { attachment, completionHandler -> completionHandler.completed(Unit, attachment) }
    }

    @Test
    fun `nioAwait error`() = runTest {
        try {
            nioAwait<Unit> { attachment, completionHandler -> completionHandler.failed(Exception("test"), attachment) }
            fail()
        } catch (e: Exception) {
            assertEquals("test", e.message)
        }
    }

    @Test
    fun `read works`() = runTest {
        val buffer = ByteBuffer.allocate(1)

        val channel = object : AsynchronousByteChannel by TestAsynchronousByteChannel {
            override fun <A> read(dst: ByteBuffer, attachment: A, handler: CompletionHandler<Int, in A>) {
                assertSame(buffer, dst)
                dst.put(42)
                handler.completed(1, attachment)
            }
        }

        val result = channel.aRead(buffer)
        assertEquals(1, result)
        buffer.flip()
        assertEquals(42, buffer.get())
    }

    @Test
    fun `read fails`() = runTest {
        val buffer = ByteBuffer.allocate(1)

        val channel = object : AsynchronousByteChannel by TestAsynchronousByteChannel {
            override fun <A> read(dst: ByteBuffer, attachment: A, handler: CompletionHandler<Int, in A>) {
                assertSame(buffer, dst)
                handler.failed(IOException("test"), attachment)
            }
        }

        try {
            channel.aRead(buffer)
            fail()
        } catch (e: Exception) {
            assertEquals("test", e.message)
        }
    }
}
