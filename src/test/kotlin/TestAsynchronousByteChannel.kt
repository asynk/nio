package io.gitlab.asynk.nio

import java.nio.ByteBuffer
import java.nio.channels.AsynchronousByteChannel
import java.nio.channels.CompletionHandler
import java.util.concurrent.Future

/**
 * Implements [AsynchronousByteChannel] but fails on each method
 */
object TestAsynchronousByteChannel : AsynchronousByteChannel {
    override fun isOpen(): Boolean = TODO()

    override fun <A> write(src: ByteBuffer, attachment: A, handler: CompletionHandler<Int, in A>) = TODO()

    override fun write(src: ByteBuffer): Future<Int> = TODO()

    override fun close() = TODO()

    override fun <A> read(dst: ByteBuffer, attachment: A, handler: CompletionHandler<Int, in A>) = TODO()

    override fun read(dst: ByteBuffer): Future<Int> = TODO()
}