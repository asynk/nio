package io.gitlab.asynk.nio

import java.util.concurrent.ArrayBlockingQueue
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.startCoroutine

fun runTest(block: suspend () -> Unit) {
    val resultQueue = ArrayBlockingQueue<Result<Unit>>(1)

    block.startCoroutine(object : Continuation<Unit> {
        override val context: CoroutineContext get() = EmptyCoroutineContext

        override fun resumeWith(result: Result<Unit>) {
            resultQueue.add(result)
        }
    })

    resultQueue.take().getOrThrow()
}
